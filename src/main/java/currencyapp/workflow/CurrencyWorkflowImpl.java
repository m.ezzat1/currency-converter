package currencyapp.workflow;

import currencyapp.activity.CurrencyConverterActivity;
import currencyapp.activity.CurrencyInversionActivity;
import io.temporal.activity.ActivityOptions;
import io.temporal.common.RetryOptions;
import io.temporal.workflow.Workflow;

import java.time.Duration;

public class CurrencyWorkflowImpl implements CurrencyWorkflow {

    // RetryOptions specify how to automatically handle retries when Activities fail.
    private final RetryOptions retryoptions = RetryOptions.newBuilder()
            .setMaximumAttempts(5)
            .build();

    private final ActivityOptions options = ActivityOptions.newBuilder()
            .setScheduleToCloseTimeout(Duration.ofSeconds(15))
            .setRetryOptions(retryoptions)
            .build();

    // ActivityStubs enable calls to Activities as if they are local methods, but actually perform an RPC.
    private final CurrencyConverterActivity currency_converter_activity = Workflow.newActivityStub(CurrencyConverterActivity.class, options);
    private final CurrencyInversionActivity currency_inversion_activity = Workflow.newActivityStub(CurrencyInversionActivity.class, options);

    private double converted_amount;
    private double inverted_amount;

    @Override
    public double convertTo(String from_currency, String to_currency, double amount)
    {
        this.converted_amount = this.currency_converter_activity.convertTo(from_currency, to_currency, amount);
        this.inverted_amount = this.currency_inversion_activity.inversionOf(this.converted_amount);
        return this.inverted_amount;
    }

    @Override
    public double getConvertedAmount() {
        return this.converted_amount;
    }

    @Override
    public double getInvertedAmount() {
        return this.inverted_amount;
    }
}

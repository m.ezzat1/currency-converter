package currencyapp.workflow;

import io.temporal.workflow.QueryMethod;
import io.temporal.workflow.WorkflowInterface;
import io.temporal.workflow.WorkflowMethod;

@WorkflowInterface
public interface CurrencyWorkflow {

    @WorkflowMethod
    double convertTo(String from_currency, String to_currency, double amount);

    @QueryMethod
    double getConvertedAmount();

    @QueryMethod
    double getInvertedAmount();
}

package currencyapp.workflow;

public class HelloWorkflowImpl implements HelloWorkflow{

    @Override
    public String hello(String name) {
        return "Hello " + name;
    }
}

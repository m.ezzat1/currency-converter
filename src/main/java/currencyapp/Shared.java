package currencyapp;

public interface Shared {
    String CURRENCY_QUEUE = "CURRENCY_QUEUE";
    String CONVERT_VERTICLE = "vertx.convert";
    String HELLO_VERTICLE = "vertx.hello";
}

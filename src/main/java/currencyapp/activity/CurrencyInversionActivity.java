package currencyapp.activity;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;

@ActivityInterface
public interface CurrencyInversionActivity {

    @ActivityMethod
    double inversionOf(double amount);
}

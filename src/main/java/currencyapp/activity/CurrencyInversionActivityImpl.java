package currencyapp.activity;

public class CurrencyInversionActivityImpl implements CurrencyInversionActivity {

    @Override
    public double inversionOf(double amount)
    {
        return 1/amount;
    }
}

package currencyapp.activity;

import config.Config;
import io.temporal.activity.Activity;
import io.temporal.activity.ActivityExecutionContext;
import io.temporal.client.ActivityCompletionClient;
import io.vertx.core.Vertx;
import io.vertx.ext.web.client.WebClient;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;

public class ForexApiCurrencyConverter implements CurrencyConverterActivity {

    private final String API_TOKEN = Config.getConfig("FOREX_API_TOKEN");
    private final String BASE_URL = Config.getConfig("FOREX_BASE_URL");


    /**
     * ActivityCompletionClient is used to asynchronously complete activities. In this example we
     * will use this client alongside with {@link
     * io.temporal.activity.ActivityExecutionContext#doNotCompleteOnReturn()} which means our
     * activity method will not complete when it returns, however is expected to be completed
     * asynchronously using the client.
     */
    private final ActivityCompletionClient completionClient;
    private final WebClient client = WebClient.create(Vertx.vertx());


    public ForexApiCurrencyConverter(ActivityCompletionClient completionClient)
    {
        this.completionClient = completionClient;
    }

    @Override
    public double convertTo(String from_currency, String to_currency, double amount) {
        ActivityExecutionContext ctx = Activity.getExecutionContext();
        byte[] task_token = ctx.getTaskToken();
        URL url = this.buildURL(from_currency, to_currency, amount);

        client.get(url.getDefaultPort(), url.getHost(), url.getFile())
                .ssl(true)
                .send()
                .onSuccess(res -> {
                    if(res.statusCode() == 200 ) {
                        double converted_amount_from_response = this.getConvertedAmountFromResponse(res.bodyAsString(), to_currency);
                        completionClient.complete(task_token, converted_amount_from_response);
                    } else {
                        completionClient.completeExceptionally(task_token, new Exception(res.body().toString()));
                    }
                 })
                 .onFailure(err -> {
                    System.out.println("Something went wrong " + err.getMessage());
                    completionClient.completeExceptionally(task_token, new Exception(err.getMessage()));
                 });
        ctx.doNotCompleteOnReturn();
        return -1;
    }

    protected URL buildURL(String from_currency, String to_currency, double amount)
    {
        String endpoint = String.format(
                "%s?api_key=%s&from=%s&to=%s&amount=%2f",
                this.BASE_URL,
                this.API_TOKEN,
                from_currency,
                to_currency,
                amount
        );
        URL url;
        try {
            url = new URL(endpoint);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        return url;
    }

    protected double getConvertedAmountFromResponse(String response, String to_currency)
    {
        JSONObject res = new JSONObject(response);
        JSONObject result = res.getJSONObject("result");
        return result.getDouble(to_currency.toUpperCase());
    }
}
package currencyapp.activity;
import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;

@ActivityInterface
public interface CurrencyConverterActivity {

    @ActivityMethod
    double convertTo(String from_currency, String to_currency, double amount);

}
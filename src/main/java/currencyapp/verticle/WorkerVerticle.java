package currencyapp.verticle;

import currencyapp.Shared;
import currencyapp.activity.CurrencyInversionActivityImpl;
import currencyapp.activity.ForexApiCurrencyConverter;
import currencyapp.workflow.CurrencyWorkflowImpl;
import currencyapp.workflow.HelloWorkflowImpl;
import io.temporal.client.ActivityCompletionClient;
import io.temporal.client.WorkflowClient;
import io.temporal.serviceclient.WorkflowServiceStubs;
import io.temporal.worker.Worker;
import io.temporal.worker.WorkerFactory;
import io.vertx.core.AbstractVerticle;

public class WorkerVerticle extends AbstractVerticle {

    @Override
    public void start() throws Exception {
        WorkflowClient client = WorkflowClient.newInstance(WorkflowServiceStubs.newInstance());
        WorkerFactory factory = WorkerFactory.newInstance(client);
        Worker worker = factory.newWorker(Shared.CURRENCY_QUEUE);
        worker.registerWorkflowImplementationTypes(CurrencyWorkflowImpl.class, HelloWorkflowImpl.class);
        ActivityCompletionClient completionClient = client.newActivityCompletionClient();
        worker.registerActivitiesImplementations(
                new ForexApiCurrencyConverter(completionClient),
                new CurrencyInversionActivityImpl()
        );
        factory.start();
    }
}

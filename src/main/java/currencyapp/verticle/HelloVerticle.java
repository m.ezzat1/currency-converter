package currencyapp.verticle;

import currencyapp.Shared;
import currencyapp.workflow.CurrencyWorkflow;
import currencyapp.workflow.HelloWorkflow;
import io.temporal.client.WorkflowClient;
import io.temporal.client.WorkflowOptions;
import io.temporal.serviceclient.WorkflowServiceStubs;
import io.vertx.core.AbstractVerticle;
import org.json.JSONObject;

public class HelloVerticle extends AbstractVerticle {

    @Override
    public void start() throws Exception
    {
        vertx.eventBus().consumer(Shared.HELLO_VERTICLE, (msg) -> {
            msg.reply(helloWorkflow("Verticle!"));
        });
    }

    String helloWorkflow(String name)
    {
        WorkflowServiceStubs service = WorkflowServiceStubs.newInstance();
        WorkflowClient client = WorkflowClient.newInstance(service);
        WorkflowOptions options = WorkflowOptions.newBuilder()
                .setTaskQueue(Shared.CURRENCY_QUEUE)
                .build();
        HelloWorkflow workflow = client.newWorkflowStub(HelloWorkflow.class, options);
        return workflow.hello(name);
    }

}

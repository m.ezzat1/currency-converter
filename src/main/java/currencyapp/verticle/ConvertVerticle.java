package currencyapp.verticle;

import currencyapp.workflow.CurrencyWorkflow;
import currencyapp.Shared;
import io.temporal.client.WorkflowClient;
import io.temporal.client.WorkflowOptions;
import io.temporal.serviceclient.WorkflowServiceStubs;
import io.vertx.core.AbstractVerticle;
import org.json.JSONObject;

public class ConvertVerticle extends AbstractVerticle {

    @Override
    public void start() throws Exception
    {
        vertx.eventBus().consumer(Shared.CONVERT_VERTICLE, (msg) -> {
            JSONObject params = new JSONObject((String)msg.body());
            String convert_from = params.getString("convertFrom");
            String convert_to = params.getString("convertTo");
            double amount = Double.parseDouble(params.getString("amount"));
            msg.reply(String.valueOf(this.conversionWorkflow(convert_from, convert_to, amount)));
        });
    }

    double conversionWorkflow(String convert_from, String convert_to, double amount)
    {
        WorkflowServiceStubs service = WorkflowServiceStubs.newInstance();
        WorkflowClient client = WorkflowClient.newInstance(service);
        WorkflowOptions options = WorkflowOptions.newBuilder()
                .setTaskQueue(Shared.CURRENCY_QUEUE)
                .build();
        CurrencyWorkflow workflow = client.newWorkflowStub(CurrencyWorkflow.class, options);
        return workflow.convertTo(convert_from, convert_to, amount);
    }
}

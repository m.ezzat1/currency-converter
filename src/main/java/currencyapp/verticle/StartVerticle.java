package currencyapp.verticle;

import currencyapp.Shared;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.MultiMap;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.json.JSONObject;

public class StartVerticle extends AbstractVerticle {

    @Override
    public void start() throws Exception {
        this.deployVerticles();
        Router router = Router.router(vertx);
        router.get("/api/convert").handler(this::convertVertx);
        router.get("/api/hello").handler(this::helloVertx);
        vertx.createHttpServer().requestHandler(router).listen(8080);
    }

    void convertVertx(RoutingContext ctx)
    {
        MultiMap params = ctx.request().params();
        JSONObject params_obj = new JSONObject();
        params_obj.put("convertTo", params.get("convertTo"));
        params_obj.put("convertFrom", params.get("convertFrom"));
        params_obj.put("amount", params.get("amount"));
        vertx.eventBus().request(Shared.CONVERT_VERTICLE,params_obj.toString(), reply -> {
            ctx.request().response().end((String)reply.result().body());
        });
    }

    void helloVertx(RoutingContext ctx)
    {
        vertx.eventBus().request(Shared.HELLO_VERTICLE, "", reply -> {
            ctx.request().response().end((String)reply.result().body());
        });
    }

    void deployVerticles()
    {
        vertx.deployVerticle(new ConvertVerticle());
        vertx.deployVerticle(new HelloVerticle());
    }

}

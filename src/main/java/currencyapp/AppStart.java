package currencyapp;

import currencyapp.activity.CurrencyInversionActivityImpl;
import currencyapp.activity.ForexApiCurrencyConverter;
import currencyapp.verticle.StartVerticle;
import currencyapp.verticle.WorkerVerticle;
import currencyapp.workflow.CurrencyWorkflowImpl;
import currencyapp.workflow.HelloWorkflowImpl;
import io.temporal.client.ActivityCompletionClient;
import io.temporal.client.WorkflowClient;
import io.temporal.serviceclient.WorkflowServiceStubs;
import io.temporal.worker.Worker;
import io.temporal.worker.WorkerFactory;
import io.vertx.core.Vertx;

public class AppStart {
    public static void main(String[] args) throws Exception {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new StartVerticle());
        vertx.deployVerticle(new WorkerVerticle());
    }
}

package config;

import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Config {

    public final static String file = "config.json";

    public static String getConfig(String key)
    {
        String content;
        try {
            content = new String ( Files.readAllBytes( Paths.get(file) ) );

            JSONObject json_object = new JSONObject(content);
            content = json_object.getString(key);
        } catch (IOException e) {
            return "";
        }
        return content;
    }

}
